package samira.radfar.examm;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by Delta-Group on 11/20/2016.
 */
public class myOpenerHelper extends SQLiteOpenHelper {

    public final static int versionNo=1;
    public final static String DBname="StoriesDB.sqlite";
    Context mycontext;


    public myOpenerHelper(Context context) {
        super(context, DBname, null, versionNo);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("create table Stories (id int,nameOfStory varchar(50),contentOfStory varchar(400)" +
                ",page int, nameOfAuthor  varchar(30),NO int,fav boolean)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public String Displaymatn(int position,String table,Context context){
        SQLiteDatabase db;
        myOpenerHelper myOpener;
        myOpener = new myOpenerHelper(context);
        db = myOpener.getWritableDatabase();
        Cursor cu=db.rawQuery("select * from "+table+" group by nameOfStory", null);
        cu.moveToPosition(position);
        String s=cu.getString(1);
        db.close();
        myOpener.close();
        return s;
    }

    public Integer countpage(String fieldd,Context context){

        SQLiteDatabase db;
        myOpenerHelper myOpener;
        myOpener = new myOpenerHelper(context);
        db = myOpener.getWritableDatabase();
        Cursor cu=db.rawQuery("select * from Stories where nameOfStory Like '%"+fieldd+"%' ", null);
        int pagestory=cu.getCount();
        return pagestory;
    }

    public String Displaymatnn(int page,String b,Context context){
        SQLiteDatabase db;
        myOpenerHelper myOpener;
        myOpener = new myOpenerHelper(context);
        db = myOpener.getWritableDatabase();
        Cursor cu=db.rawQuery("select * from Stories where nameOfStory='"+b+"' and page="+page , null);
        cu.moveToFirst();
        String s=cu.getString(2);
        return s;
    }

}




