package samira.radfar.examm;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity {

        ImageView ivListOfStories,ivSearch;
        StringRequest jsonStringRequest;
        String url,flag;
        JSONArray jsonArray;
        JSONObject mainJson;
        myOpenerHelper myopener;
        public SQLiteDatabase db;
        public Cursor cursor;
        LinearLayout llcenter ;
        ListView lvListOfStories;
        storiesMyAdapter adapter;
        ImageButton ivExit;
        SharedPreferences shrPrfs,preferences;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_main);

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

            ActionBar actionBar = getSupportActionBar();
            actionBar.hide();

            preferences = getSharedPreferences("myPrfs", MODE_PRIVATE);
            flag = preferences.getString("lib", "0");

            if (!flag.equals("1")) {
                ReadANDwriteTOlclDevice();
            }

            ivListOfStories = (ImageView) findViewById(R.id.ivListOfStories);
            //Button btnListOfStories = (Button) findViewById(R.id.btnListOfStories);
            //Button btnAddStory  = (Button) findViewById(R.id.btnAddStory);
            //Button btnFavarites = (Button) findViewById(R.id.btnFavarites);
            ivSearch = (ImageView) findViewById(R.id.ivSearch);
            //Button btnSearch = (Button) findViewById(R.id.btnSearch);

            Button btnHelp = (Button) findViewById(R.id.btnHelp);
            //Button btnSettings = (Button) findViewById(R.id.btnSettings);
            //ivSettings = (ImageView) findViewById(R.id.ivSettings);
            //Button btnExit = (Button) findViewById(R.id.btnExit);
            ivExit = (ImageButton) findViewById(R.id.ivExit);

            ivListOfStories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   showListOfStories();
                }
            });

            lvListOfStories = (ListView) findViewById(R.id.lvListOfStories);
            lvListOfStories.setClickable(true);

            lvListOfStories.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    lvListOfStories.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
                    lvListOfStories.setSelector(android.R.color.holo_green_light);

                    Intent intent = new Intent(MainActivity.this, Content.class);
                    intent.putExtra("key", position);
                    startActivity(intent);
                }
            });

            ivSearch.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    searchMyDialog dialog = new searchMyDialog();
                    dialog.show(getFragmentManager(),"search");
                    //dialog.setLayout(600, 400);
                }
            });

            ivExit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });

        }

//-----------------------------------------------showListOfStories()--------------------------------
    public void showListOfStories(){

        llcenter = (LinearLayout) findViewById(R.id.llCenter);
        llcenter.setBackgroundResource(R.drawable.bamboo_frame);
        lvListOfStories = (ListView) findViewById(R.id.lvListOfStories);
        lvListOfStories.setVisibility(View.VISIBLE);

        myopener = new myOpenerHelper(MainActivity.this);
        db = myopener.getWritableDatabase();
        cursor = db.rawQuery("select nameOfStory,nameOfAuthor from Stories Group by nameOfStory",null);

        adapter = new storiesMyAdapter();

        int i=1;
        while (cursor.moveToNext()){
            String b = cursor.getString(0);
            String c = cursor.getString(1);
            Stories story = new Stories(i,b,c);
            i++;
            adapter.addItem(story);
            lvListOfStories.setAdapter(adapter);
        }

        myopener.close();
        db.close();
    }

//-----------------------------------------------ReadANDwriteTOlclDevice--------------------------------
    public void ReadANDwriteTOlclDevice(){

        url = "http://razmkhah.ir/webservice/getBooks";
        jsonStringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    mainJson = new JSONObject(response);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                try {
                    jsonArray = mainJson.getJSONArray("result");
                    int length = jsonArray.length();
                    for (int j=0; j<length; j++){
                        int id =jsonArray.getJSONObject(j).getInt("id");
                        String nameOfStory = jsonArray.getJSONObject(j).getString("Namestory");
                        String ContentOfStory = jsonArray.getJSONObject(j).getString("contentstory");
                        int page =jsonArray.getJSONObject(j).getInt("page");
                        String nameOfAuthor = jsonArray.getJSONObject(j).getString("authorstory");
                        int NO =jsonArray.getJSONObject(j).getInt("numstory");
                        Boolean fav = false;
                        WriteToDB(id,nameOfStory,ContentOfStory,page,nameOfAuthor,NO,fav);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String> params = new HashMap<String, String>();
                params.put("apikey","5ee555f906f9320cad74b58ee6ef3b47d7d2c246");
                return params;
            }
        };
        Volley.newRequestQueue(MainActivity.this).add(jsonStringRequest);
        flag = preferences.getString("lib", "1");
    }

//-----------------------------------------------WriteToDB------------------------------------------
    public void WriteToDB(int id, String namestory, String contentstory, int page,
                          String authorstory, int numberstory, Boolean fa){

        myopener = new myOpenerHelper(MainActivity.this);
        db = myopener.getWritableDatabase();
        db.execSQL("insert into Stories (id,nameOfStory,contentOfStory,page,nameOfAuthor,NO,fav)values('" + id + "','" + namestory + "','" + contentstory + "','" + page + "','" + authorstory + "','" + numberstory + "','" + fa + "');");
        db.close();
        myopener.close();

    }

//-----------------------------------------------myAdapter Class------------------------------------
public class storiesMyAdapter extends BaseAdapter {

    private ArrayList<Stories> array_Stories = new ArrayList<>();
    TextView tvNO,tvNameOfStory,tvNameOfAuthor;

    public void addItem(Stories item) {
        array_Stories.add(item);
    }

    @Override
    public int getCount() { return array_Stories.size();
    }

    @Override
    public Object getItem(int position) {
        return array_Stories.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = getLayoutInflater() ;
        View v = inflater.inflate(R.layout.tablerow, parent, false);

        tvNO = (TextView) v.findViewById(R.id.tvNO);
        tvNO.setText(String.valueOf(array_Stories.get(position)._NO));

        tvNameOfStory = (TextView) v.findViewById(R.id.tvNameOfStory);
        tvNameOfStory.setText(array_Stories.get(position)._story);

        tvNameOfAuthor = (TextView) v.findViewById(R.id.tvNameOfAuthor);
        tvNameOfAuthor.setText(array_Stories.get(position)._author);

        return v;
    }
}


//-----------------------------------------------searchMyAdapter class---------------------------------------------------

    public class searchMyAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return 0;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            EditText et;
            RadioGroup rgSearch;
            RadioButton rbStory,rbContent;
            ImageView ivSearch;
            ListView lvResult;

            LayoutInflater inflater = getLayoutInflater();
            View view = inflater.inflate(R.layout.search,parent,false);



            return view;
        }
    }

    }

