package samira.radfar.examm;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Delta-Group on 11/21/2016.
 */
public class searchMyDialog extends DialogFragment {

    private EditText etWord;
    private RadioGroup rgSearch;
    private RadioButton rbStory,rbcontent;
    private ImageView ivSearch;
    private ListView lvResult;
    private myOpenerHelper myopener;
    private SQLiteDatabase db;
    private Cursor cursor;
    private View view;
    private searchMyAdapter adapter;
    private TextView tvNOresult,tvNameOfStory;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setTitle("Search");
        LayoutInflater inflater = getActivity().getLayoutInflater();
        view = inflater.inflate(R.layout.search,null);

        rgSearch = (RadioGroup) view.findViewById(R.id.rgSearch);
        rbStory = (RadioButton) view.findViewById(R.id.rbStory);
        rbcontent = (RadioButton) view.findViewById(R.id.rbContent);
        ivSearch = (ImageView) view.findViewById(R.id.ivSearch);
        etWord = (EditText) view.findViewById(R.id.etWord);
        lvResult = (ListView) view.findViewById(R.id.lvResult);
        tvNOresult = (TextView) view.findViewById(R.id.tvNOresult);
        tvNameOfStory = (TextView) view.findViewById(R.id.tvNameOfStory);

        myopener = new myOpenerHelper(getActivity());
        db = myopener.getWritableDatabase();

        ivSearch.setOnClickListener(new View.OnClickListener() {
                       @Override
            public void onClick(View v) {

                  if (etWord.getText().equals("")) {
                               tvNOresult.setText("کلمه جستجو را وارد کنید.");
                  } else {
                         adapter = new searchMyAdapter();
                      String word=etWord.getText().toString();
                         if (rbStory.isChecked()) {
                            cursor = db.rawQuery("select nameOfStory from Stories where nameOfStory like '%" + word + "%' Group by nameOfStory", null);
                             if (cursor.getCount() == 0) {
                                 //lvResult.setEmptyView(lvResult);
                                 tvNOresult.setText("نتیجه ای یافت نشد.");
                             } else {
                                 tvNOresult.setText( cursor.getCount() + "نتیجه یافت شد.");
                                 //int i = 1;
                                  while (cursor.moveToNext()) {
                                      String name= cursor.getString(0);
                                      adapter.addItem(name);
                                      lvResult.setAdapter(adapter);
                                       }
                                   }
                               }
                         else if (rbcontent.isChecked()) {
                                   cursor = db.rawQuery("select nameOfStory,contentOfStory from Stories where contentOfStory like '%" + word + "%' Group by nameOfStory", null);
                             if (cursor.getCount() == 0) {
                                 tvNOresult.setText("نتیجه ای یافت نشد.");
                             }
                             else {
                                 tvNOresult.setText(cursor.getCount() + " نتیجه یافت شد.");
                                 while (cursor.moveToNext()) {
                                     String content= cursor.getString(1);
                                     adapter.addItem(content);
                                     lvResult.setAdapter(adapter);
                                 }
                             }
                               } else {
                                    tvNOresult.setText("یک نوع جستجو انتخاب کنید.");
                         }
                           }
                       }
        });

        builder.setView(view);
        //.getWindow().setLayout(600, 400);
        return builder.create();
    }

//-----------------------------------------------searchMyAdapter------------------------------------
public class searchMyAdapter extends BaseAdapter {

    TextView tvResult;
    private ArrayList<String> resultArray = new ArrayList<String>();
   // private ArrayList<Stories> array_Stories = new ArrayList<>();

    public void addItem(String item) {
        resultArray.add(item);
    }

    @Override
    public int getCount() {
        return resultArray.size();
    }

    @Override
    public Object getItem(int position) {
        return resultArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = getActivity().getLayoutInflater() ;
        View view = inflater.inflate(R.layout.result_row, parent, false);

        tvResult = (TextView) view.findViewById(R.id.tvResult);
        tvResult.setText(resultArray.get(position));

        return view;
    }
}
}
