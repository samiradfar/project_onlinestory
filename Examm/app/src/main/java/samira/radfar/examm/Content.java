package samira.radfar.examm;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

public class Content extends AppCompatActivity {

    ImageView ivNext,ivback;
    TextView tvContent;
    myOpenerHelper myOpener;
    SQLiteDatabase db;
    String name;
    int position,pagekol,Page2=1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);

        ActionBar actionBar = getSupportActionBar();
        actionBar.hide();

        ivNext = (ImageView) findViewById(R.id.ivnext);
        ivback = (ImageView) findViewById(R.id.ivback);
        tvContent = (TextView) findViewById(R.id.tvContent);

        //Typeface titrf = Typeface.createFromAsset(getAssets(), "fonts/nazanin.ttf");
        //tvContent.setTypeface(titrf);

        Bundle extras = getIntent().getExtras();
        name = extras.getString("namestory");

        if (extras != null) {
            position = extras.getInt("key");
        }

        pagekol = ReadDbpagekol(position);
        ReadDbmatn(position, Page2);

        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pagekol == Page2) {
                    Toast.makeText(getApplicationContext(), "آخرین صفحه", Toast.LENGTH_SHORT).show();
                } else {
                    Page2++;
                    ReadDbmatn(position, Page2);
                }
            }
        });

        ivback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Page2 == 1) {
                    Toast.makeText(getApplicationContext(), "اولین صفحه", Toast.LENGTH_SHORT).show();
                } else {
                    Page2--;
                    ReadDbmatn(position, Page2);
                }
            }
        });

    }
    public int ReadDbpagekol(int position) {

        myOpener = new myOpenerHelper(this);
        //db = myOpener.getWritableDatabase();
        String b = myOpener.Displaymatn(position,"Stories",this);
        pagekol = myOpener.countpage(b,this);
       // db.close();
        myOpener.close();
        return pagekol;
    }

    public Cursor ReadDbmatn(int position, int page2) {
        myOpener = new myOpenerHelper(this);
        String b = myOpener.Displaymatn(position, "Stories",this);
        String bmatn = myOpener.Displaymatnn(page2, b,this);
        tvContent.setText(bmatn);
        myOpener.close();
        return null;
    }
}
